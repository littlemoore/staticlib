CC = gcc
LTRG = libtest.a
OTRG = hello

.PHONY: all
all: test1.o test2.o $(LTRG) main


main:
#	$(CC)  -o $(OTRG) main.c $(LTRG)
	$(CC) main.c -L ./ -ltest -o $(OTRG)
test1: test1.o
	$(CC) -c test1.c 

test2: test2.o
	$(CC) -c test2.c 

$(LTRG):
	ar cqs $(LTRG) test1.o test2.o


.PHONY: clean
clean :
	rm *.o $(LTRG) $(OTRG)                                

